import React, {Component} from 'react';
import axios from "../../axios-feedback";
import Spinner from "../../components/UI/Spinner/Spinner";
import withLoader from "../../components/hoc/withErrorHandler/withErrorHandler";
import './Contacts.css';

class Contacts extends Component {
    state = {
        contacts: null,
        loading: true
    };

    loadData() {
        let url = '/contacts.json';
        axios.get(url).then(response => {
            this.setState({contacts: response.data, loading: false})
            });
    }

    componentDidMount() {
        this.loadData();
    }
    render() {
        if (this.state.loading) {
            return <Spinner />
        }
        return (
            <div className="Contacts">
                <img src={this.state.contacts.image} alt="contact"/>
                <p><strong>Phone: </strong>{this.state.contacts.phone}</p>
                <p><strong>FAX: </strong>{this.state.contacts.fax}</p>
                <p><strong>Address: </strong>{this.state.contacts.address}</p>
            </div>
        );
    }
}

export default withLoader(Contacts, axios);