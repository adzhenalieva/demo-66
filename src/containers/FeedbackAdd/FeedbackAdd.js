import React, {Component, Fragment} from 'react';
import axios from '../../axios-feedback';
import FeedbackForm from "../../components/FeedbackForm/FeedbackForm";

class AddFeedback extends Component {
    addFeedback = product => {
            axios.post('feedback.json', product).then(() => {
            this.props.history.replace('/');
        })
    };

    render() {
        return (
            <Fragment>
                <h1>Add your feedback</h1>
                <FeedbackForm onSubmit={this.addFeedback}/>
            </Fragment>
        );
    }
}

export default AddFeedback;