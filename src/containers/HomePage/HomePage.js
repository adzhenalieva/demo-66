import React, {Component} from 'react';
import './HomePage.css';
import Gallery from "../../components/Carousel/Carousel";
import axios from '../../axios-feedback';
import { CardColumns } from "reactstrap";
import withLoader from "../../components/hoc/withErrorHandler/withErrorHandler";
import ErrorBoundary from "../../components/ErrorBoundary/ErrorBoundary";
import Feedback from "../../components/FeedbackCard/Feedback";
import Spinner from "../../components/UI/Spinner/Spinner";


class HomePage extends Component {
    state = {
        feedback: null
    };

    loadData() {
        let url = 'feedback.json';
        axios.get(url).then(response => {
            const feedback = Object.keys(response.data).map(id => {
                return {...response.data[id], id}
            });
            this.setState({feedback})
        })
    }

    componentDidMount() {
        this.loadData();
    }

    render() {
        let feedback = <Spinner />;
        if (this.state.feedback) {
            feedback = this.state.feedback.map((feedback) => (
                <ErrorBoundary key={feedback.id}>
                <Feedback image = {feedback.image}
                          author = {feedback.author}
                          text = {feedback.text}/>
                </ErrorBoundary>
            ));
        }
        return (
            <div className="Container">
                <h1 className="Title">Now is better than never</h1>
                <Gallery/>
                <h2 className="Title">Feedback from our clients</h2>
                <CardColumns>
                    {feedback}
                </CardColumns>
            </div>
        );
    }
}

export default withLoader(HomePage, axios);