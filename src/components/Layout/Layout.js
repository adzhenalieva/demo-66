import React, {Fragment} from 'react';
import Toolbar from "../Toolbar/Toolbar";


const Layout = ({children}) => {
    return (
        <div>
            <Fragment>
                <Toolbar/>
                <main className="Layout-Content">{children}</main>
            </Fragment>
        </div>
    );
};

export default Layout;