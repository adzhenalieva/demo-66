import React from 'react';
import { Jumbotron } from 'reactstrap';

const About = () => {
    return (
        <div>
            <Jumbotron>
                <h1 className="display-3">About us</h1>
                <p className="lead">There are many benefits to taking an open online course!
                    It’s free!
                    It’s online, so you can learn from anywhere.
                    Many times, you can earn a certificate that shows you completed a course.
                    Many times, the course is taught by real professors from actual universities. You can even take a course from a Harvard professor for free!
                    You can drop out anytime. Unlike with an actual university, it doesn’t matter if you suddenly don’t have enough time to finish your class. You can start and stop anytime and you won’t lose anything.
                    Did we mention it’s free?</p>
                <hr className="my-2" />
                <p>The Canvas Network is a website for “lifelong learners,” or people who want to continue learning even when they’re finished with school. They have courses from a lot of universities, and many of the classes are very professional.

                    This website encourages students to learn along with the class and have conversations about the lessons. If you prefer to learn on your own, you can do that, too. All the material is available for you to see once a course is over.</p>
                <p className="lead">Stay with us</p>
                <img src="https://www.fluentu.com/blog/english/wp-content/uploads/sites/4/2015/05/online-english-courses4-150x150.png" alt="learning"/>
            </Jumbotron>
        </div>
    );
};

export default About;