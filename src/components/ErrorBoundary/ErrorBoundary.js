import React, {Component} from 'react';
import axios from '../../axios-feedback';


class ErrorBoundary extends Component {
    state = {
        hasError: false,
        errorMessage: '',
    };

    componentDidCatch = (error, info) => {
        axios.post('error.json', {error: error.message}).then(() => {
            this.setState({hasError: true, errorMessage: error.message});
        });

    };

    render() {

        if (this.state.hasError) {
            return <div>
                <br/>
                <br/>
                {this.state.errorMessage}
            </div>
        } else {
            return this.props.children;
        }
    }
}

export default ErrorBoundary;