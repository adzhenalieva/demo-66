import React from 'react';
import {Nav, NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

const Toolbar = () => {
    return (
        <div className="clearfix mt-2 mb-5 mr-4">
            <Nav pills className="float-right">
                <NavItem>
                    <NavLink tag={RouterNavLink} to={"/"} exact>Home</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={RouterNavLink} to={"/about"}>About</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={RouterNavLink} to={"/add"}>Give feedback</NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={RouterNavLink} to={"/contacts"}>Contacts</NavLink>
                </NavItem>
            </Nav>
        </div>
    );
};

export default Toolbar;