import React, {Component} from 'react';
import {Card, CardBody, CardImg, CardText, CardTitle} from "reactstrap";



class Feedback extends Component {


    componentDidMount() {
        if (Math.random() > 0.8){
            throw new Error('This is special error');
        }
    }

    render() {
        return (
            <Card className="mt-4">
                <CardBody>
                    <CardImg top width="100%" src={this.props.image} alt="Card image cap"/>
                    <CardText className="mt-4">{this.props.text}</CardText>
                    <CardTitle><strong>{this.props.author}</strong></CardTitle>
                </CardBody>
            </Card>
        );
    }
}

export default Feedback;