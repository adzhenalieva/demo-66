import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";

class FeedbackForm extends Component {
    state = {
        author: '',
        text: '',
        image: '',

    };

    valueChanged = event => {
        const {name, value} = event.target;
        this.setState({[name]: value})
    };

    submitHandler = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };

    render() {

        return (
            <div>
                <Form onSubmit={this.submitHandler}>
                    <FormGroup row>
                        <Label sm={2}>Author</Label>
                        <Col sm={10}>
                            <Input type="text" name="author" value={this.state.author}
                                   onChange={this.valueChanged}/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2}>Image</Label>
                        <Col sm={10}>
                            <Input type="url" name="image" value={this.state.image}
                                   onChange={this.valueChanged}/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2}>Text</Label>
                        <Col sm={10}>
                            <Input type="textarea" name="text" value={this.state.text}
                                   onChange={this.valueChanged}/>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Col sm={{size: 10, offset: 2}}>
                    <Button type="submit">Save</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

export default FeedbackForm;