import React, {Component} from 'react';
import Layout from "./components/Layout/Layout";
import {Switch, Route} from "react-router-dom";
import HomePage from "./containers/HomePage/HomePage";
import Container from "reactstrap/es/Container";
import About from "./components/About/About";
import AddFeedback from "./containers/FeedbackAdd/FeedbackAdd";
import Contacts from "./containers/Contacts/Contacts";

class App extends Component {
    render() {
        return (
            <Layout>
                <Container>
                    <Switch>
                        <Route path="/" exact component={HomePage}/>
                        <Route path="/add" component={AddFeedback}/>
                        <Route path="/about" component={About}/>
                        <Route path="/contacts" component={Contacts}/>

                    </Switch>
                </Container>
            </Layout>
        );
    }
}

export default App;
